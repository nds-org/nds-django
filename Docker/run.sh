#!/bin/bash

docker run -d --name rabbitmq dockerfile/rabbitmq:latest
docker run -d --name db training/postgres:latest
docker run -d --name nds-api --link db:db --link rabbitmq:rabbitmq -p 80 keyz182/nds_rest_api:latest
docker run -d --privileged --name nds-worker --link db:db --link rabbitmq:rabbitmq -p 80 keyz182/nds_rest_worker:latest

