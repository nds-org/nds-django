#!/bin/bash

python /home/docker/code/manage.py syncdb --noinput
python /home/docker/code/manage.py loaddata admin.json

supervisord -n

