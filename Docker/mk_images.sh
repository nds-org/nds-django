#!/bin/bash

rm -rf build
git clone git@bitbucket.org:keyz182/nds-django.git build
pushd build &> /dev/null
git submodule init
git submodule update
pushd ythub_workers &> /dev/null
git pull origin master
popd &> /dev/null
popd &> /dev/null

cp Dockerfile.api Dockerfile
docker build -t keyz182/nds_rest_api:latest .
rm Dockerfile

cp Dockerfile.worker Dockerfile
docker build -t keyz182/nds_rest_worker:latest .
rm Dockerfile

rm -rf build