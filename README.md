NDS Django REST interface for Docker
====================================


```bash
$ pip install -r requirements.txt
$ ./manage.py syncdb
$ ./manage.py runserver
$ #In another terminal
$ ./manage.py celeryd -E -l INFO
```

OR

```bash
$ docker run -d --name rabbitmq dockerfile/rabbitmq:latest
$ docker run -d --name db training/postgres:latest
$ docker run -d --name nds-api --link db:db --link rabbitmq:rabbitmq -p 80 keyz182/nds_rest_api:latest
$ docker run -d --privileged --name nds-worker --link db:db --link rabbitmq:rabbitmq -p 80 keyz182/nds_rest_worker:latest
```


```bash
$ curl -F "image_name=ytproject/yt-devel" \
       -F "image_tag=hublaunch" \
       -F "file=@script.py" \
       -F "data=IsolatedGalaxy" \
       "http://localhost:8000/v1/upload"
$ # OR
$ curl -F "image_name=ytproject/yt-devel" \
       -F "image_tag=hublaunch" \
       -F "script=print('Hello World')" \
       -F "scriptname=script.py" \
       -F "data=IsolatedGalaxy" \
       "http://localhost:8000/v1/upload"
       
$ curl "http://localhost:8000/v1/stdout?job_id=997231c3ce6"
$ curl "http://localhost:8000/v1/diff?job_id=997231c3ce6"
$ curl "http://localhost:8000/v1/metric?job_id=997231c3ce6"
$ curl "http://localhost:8000/v1/download?job_id=997231c3ce6&path=/results"
```