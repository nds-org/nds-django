import os
from django.core.management.base import BaseCommand
from optparse import make_option
import string

scaffolding = {}

scaffolding['docker_rest/models.py'] = \
r"""
class %(task_name)s
    created = models.DateTimeField(auto_now_add=True)
    data = models.CharField(max_length=1024*1024, null=True, blank=True)
    task_id = models.CharField(max_length=128, blank=True, default='')
"""

scaffolding['docker_rest/serializers.py'] = \
r"""
class %(task_name)sSerializer(serializers.ModelSerializer):
    class Meta:
        model = %(task_name)s
        fields = ('id','created','data','job',)
"""

scaffolding['docker_rest/urls.py'] = \
r"""
urlpatterns += format_suffix_patterns([
    url(r'^%(url)s$', %(task_name)s.as_view())
])
"""

scaffolding['docker_rest/views.py'] = \
r"""
class %(task_name)sView(TemplateJob):
    def __init__(self, *args, **kwargs):
        self.queryset = %(task_name)s.objects.all()
        self.serializer_class = %(task_name)sSerializer
        self.permission_classes = (permissions.AllowAny,)
        self.task_name = 'ythub_workers.tasks.%(task_name_celery)s'

    def build_kwargs(self,obj):
        return {}
"""

scaffolding['ythub_workers/tasks.py'] = \
r"""
@shared_task
def %(task_name_celery)s(*args, **kwargs):
    return json.dumps({})
"""

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--celery-name',
            action='store',
            dest='celery_name'),
        make_option('--name',
            action='store',
            dest='name'),
        make_option('--url',
            action='store',
            dest='url'),
        )

    def handle(self, *args, **options):
        if options['name'] is None:
            self.stdout.write("--name is required")
            return
        if options['name'] not in string.uppercase:
            options['name'] = options['name'].capitalize()
        if options['celery_name'] is None:
            options['celery_name'] = options['name'].lower()
        if options['url'] is None:
            options['url'] = options['celery_name']

        fmt = {'task_name': options['name'],
               'task_name_celery': options['celery_name'],
               'url': options['url']}

        for k, v in sorted(fmt.items()):
            self.stdout.write("Set %s to %s" % (k, v))

        base_path = os.path.dirname(os.path.realpath(__file__))
        self.stdout.write(base_path)
        for fn, b in sorted(scaffolding.items()):
            self.stdout.write("Adding to %s" % fn)
            with open(fn, "a") as f:
                f.write("\n\n### Added by rest_endpoint_helper for %s\n" % options['name']) # Add a newline
                f.write(b % fmt)
                f.write("\n###\n\n") # Add a newline