__author__ = 'keyz'
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from views import \
    UploadJob, StatusJob, StdOutJob, MetricsJob, DiffJob, ListDataJob, \
    ListImagesJob, ListContainersJob, DownloadJob, InfoContainerJob, \
    StopContainerJob

urlpatterns = [
    url(r'^upload$', UploadJob.as_view()),
    url(r'^status$', StatusJob.as_view()),
    url(r'^stdout$', StdOutJob.as_view()),
    url(r'^metric$', MetricsJob.as_view()),
    url(r'^diff$', DiffJob.as_view()),
    url(r'^list_data$', ListDataJob.as_view()),
    url(r'^list_images$', ListImagesJob.as_view()),
    url(r'^list_container$', ListContainersJob.as_view()),
    url(r'^download$', DownloadJob.as_view()),
    url(r'^info$', InfoContainerJob.as_view()),
    url(r'^stop$', StopContainerJob.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
