# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('docker_rest', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobStdOut',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('data', models.CharField(max_length=1048576, null=True, blank=True)),
                ('job', models.ForeignKey(related_name='stdouts', to='docker_rest.Job')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='jobmetric',
            name='metric_id',
        ),
        migrations.RemoveField(
            model_name='jobmetric',
            name='state',
        ),
        migrations.RemoveField(
            model_name='jobstate',
            name='state',
        ),
        migrations.RemoveField(
            model_name='jobstate',
            name='state_id',
        ),
    ]
