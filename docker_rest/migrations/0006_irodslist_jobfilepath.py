# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('docker_rest', '0005_auto_20141028_1015'),
    ]

    operations = [
        migrations.CreateModel(
            name='IrodsList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('data', models.CharField(max_length=1048576, null=True, blank=True)),
                ('task_id', models.CharField(default=b'', max_length=128, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobFilePath',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('data', models.CharField(max_length=1048576, null=True, blank=True)),
                ('task_id', models.CharField(default=b'', max_length=128, blank=True)),
                ('job', models.ForeignKey(related_name='filepaths', to='docker_rest.Job')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
