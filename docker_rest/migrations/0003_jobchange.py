# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('docker_rest', '0002_auto_20141027_1437'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobChange',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('data', models.CharField(max_length=1048576, null=True, blank=True)),
                ('job', models.ForeignKey(related_name='changes', to='docker_rest.Job')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
