# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('docker_rest', '0006_irodslist_jobfilepath'),
    ]

    operations = [
        migrations.DeleteModel(
            name='IrodsList',
        ),
        migrations.RemoveField(
            model_name='jobchange',
            name='job',
        ),
        migrations.DeleteModel(
            name='JobChange',
        ),
        migrations.RemoveField(
            model_name='jobmetric',
            name='job',
        ),
        migrations.DeleteModel(
            name='JobMetric',
        ),
        migrations.RemoveField(
            model_name='jobstate',
            name='job',
        ),
        migrations.DeleteModel(
            name='JobState',
        ),
        migrations.RemoveField(
            model_name='jobstdout',
            name='job',
        ),
        migrations.DeleteModel(
            name='JobStdOut',
        ),
        migrations.AddField(
            model_name='jobfilepath',
            name='path',
            field=models.CharField(max_length=1024, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='job',
            name='scriptname',
            field=models.CharField(default=b'script.py', max_length=256),
            preserve_default=True,
        ),
    ]
