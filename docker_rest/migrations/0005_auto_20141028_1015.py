# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('docker_rest', '0004_auto_20141027_1508'),
    ]

    operations = [
        migrations.AddField(
            model_name='jobchange',
            name='task_id',
            field=models.CharField(default=b'', max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobmetric',
            name='task_id',
            field=models.CharField(default=b'', max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobstate',
            name='task_id',
            field=models.CharField(default=b'', max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobstdout',
            name='task_id',
            field=models.CharField(default=b'', max_length=128, blank=True),
            preserve_default=True,
        ),
    ]
