from celery.utils import worker_direct

__author__ = 'keyz'


class YTHubRouter(object):

    def get_routing_key(hostname):
        queue = worker_direct(hostname)
        return queue.name

    def get_exchange(hostname):
        queue = worker_direct(hostname)
        return queue.exchange.name

    def route_for_task(self, task, args=None, kwargs=None):
        if(task != 'ythub_workers.tasks.create_job_docker'):
            # Other tasks need to go to the correct worker.
            # User routing key for this.
            if('worker' in kwargs):
                worker = kwargs['worker']
                queue = worker_direct(worker)

                routing_info = {
                    'exchange': queue.exchange.name,
                    'queue': worker}
                # 'routing_key': queue.name}

                return routing_info
