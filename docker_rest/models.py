from django.db import models

# Create your models here.


class Job(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    scriptname = models.CharField(max_length=256, default='script.py')
    # Do we want to store this here? Text/Binary? 1MB?
    script = models.CharField(max_length=1024 * 1024, blank=False)
    image_name = models.CharField(max_length=256, blank=False)
    image_tag = models.CharField(max_length=256, blank=True)

    worker = models.CharField(max_length=64, blank=True)
    create_task_id = models.CharField(max_length=128, blank=True)
    create_task_state = models.CharField(max_length=16, null=True, blank=True)
    container_id = models.CharField(max_length=128, null=True, blank=True)

    state = models.CharField(max_length=16, null=True, blank=True)

    owner = models.ForeignKey(
        'auth.User', related_name='jobs', null=True, blank=True)

    class Meta:
        ordering = ('created',)


class JobFilePath(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    data = models.CharField(max_length=1024 * 1024, null=True, blank=True)
    path = models.CharField(max_length=1024, null=True, blank=True)
    task_id = models.CharField(max_length=128, blank=True, default='')

    job = models.ForeignKey(Job, related_name='filepaths')
