# from celery.result import AsyncResult
from django.shortcuts import get_object_or_404
# from rest_framework.decorators import api_view
from rest_framework.response import Response
import simplejson
from django.contrib.auth.models import User, AnonymousUser
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework import views
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics
# from django.http import HttpResponseServerError

from nds_django import app as celery

# from docker_rest.permission import IsOwnerOrReadOnly
from docker_rest.serializers import \
    JobSerializer, UserSerializer, JobFilePathSerializer
from docker_rest.models import \
    Job, JobFilePath


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CreateRetrieveAPIView(mixins.RetrieveModelMixin,
                            mixins.CreateModelMixin,
                            generics.GenericAPIView):
    """
    Concrete view for listing a queryset or creating a model instance.
    """
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class UploadJob(CreateRetrieveAPIView):
    """
    To create a Job, you must POST scriptname, script,
    image_name and image_tag (all strings).

    You will receive an ID in return.

    GET a job resource with that ID and the API
    will check to see if the job has finished.

    It will give various details such as the container ID and worker.
    """
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.AllowAny,)

    def retrieve(self, request,  *args, **kwargs):
        if 'id' in request.QUERY_PARAMS:
            id = request.QUERY_PARAMS['id']
        else:
            return Response("No create_task_id specified",
                            status=status.HTTP_400_BAD_REQUEST)

        job = get_object_or_404(Job, id=id)

        ret = celery.AsyncResult(job.create_task_id)
        job.create_task_state = ret.state

        if ret.state == "SUCCESS":
            try:
                res = simplejson.loads(ret.get())
                job.container_id = ret['Id']
                job.worker = ret['worker']
            except Exception as e:
                res = e.message
                # TODO do something with 'res'

        job.save()

        serializer = self.get_serializer(job)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = request.DATA

        if 'script' not in data:
            if 'file' not in request.FILES:
                return Response("No script uploaded",
                                status=status.HTTP_400_BAD_REQUEST)
            data['script'] = request.FILES['file'].read()
            if 'scriptname' not in data:
                data['scriptname'] = 'script.py'

        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)

            headers = self.get_success_headers(serializer.data)

            ret_data = serializer.data
            del ret_data['script']
            return Response(ret_data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def pre_save(self, obj):
        if not isinstance(self.request.user, AnonymousUser):
            obj.owner = self.request.user

    def post_save(self, obj, created):
        ret = celery.send_task('ythub_workers.tasks.create_job_docker',
                               kwargs={
                                   'script': obj.script,
                                   'scriptname': obj.scriptname,
                                   'image_name': obj.image_name,
                                   'image_tag': obj.image_tag})

        obj.create_task_id = ret.id
        obj.save()


class TemplateJob(CreateRetrieveAPIView):
    queryset = None
    serializer_class = None
    permission_classes = None
    task_name = None

    def list(self, request, job_pk=None):
        objects = self.queryset.filter(job=job_pk)
        ser = self.serializer_class(objects, many=True)
        return Response(ser.data)

    def send_job(self, job_pk):
        data = {'job': job_pk}

        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            serializer.save()
            self.post_save(serializer.object, True)
            return {'valid': True, 'serializer': serializer}

        return {'valid': False, 'serializer': serializer}

    def create(self, request):
        if 'job_id' in request.QUERY_PARAMS:
            job_pk = request.QUERY_PARAMS['job_id']
        else:
            return Response("No Job ID Specified",
                            status=status.HTTP_400_BAD_REQUEST)

        ret = self.send_job(job_pk)
        if ret['valid']:
            return Response(ret['serializer'].data,
                            status=status.HTTP_201_CREATED)
        return Response(ret['serializer'].errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request):
        if 'job_id' in request.QUERY_PARAMS:
            job_pk = request.QUERY_PARAMS['job_id']
        else:
            return Response("No Job ID Specified",
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            object = self.queryset.get(job_pk=job_pk)
            ser = self.serializer_class(object)
            return Response(ser.data)
        except:
            ret = self.send_job(job_pk)
            if ret['valid']:
                return Response(ret['serializer'].data,
                                status=status.HTTP_200_OK)
            return Response(ret['serializer'].errors,
                            status=status.HTTP_400_BAD_REQUEST)

    def build_kwargs(self, obj):
        return None

    def post_save(self, obj, created):
        kwargs = self.build_kwargs(obj)
        kwargs['worker'] = obj.job.worker

        ret = celery.send_task(self.task_name, kwargs=kwargs)
        obj.task_id = ret.id
        try:
            obj.data = simplejson.loads(ret.get())
        except Exception as e:
            obj.data = e.message

        obj.save()


class MetricsJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.get_job_metric_docker',
                               kwargs=self.request.QUERY_PARAMS)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class StatusJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.get_job_state_docker',
                               kwargs=self.request.QUERY_PARAMS)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class StdOutJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.get_job_stdout_docker',
                               kwargs=self.request.QUERY_PARAMS)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class DiffJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.get_job_changes_docker',
                               kwargs=self.request.QUERY_PARAMS)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class ListDataJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.get_list_data_irods',
                               kwargs=kwargs)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class InfoContainerJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.get_container_info',
                               kwargs=self.request.QUERY_PARAMS)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class StopContainerJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.stop_container_docker',
                               kwargs=self.request.QUERY_PARAMS)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class ListImagesJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.get_docker_image_list',
                               kwargs=kwargs)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class ListContainersJob(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        ret = celery.send_task('ythub_workers.tasks.list_containers_docker',
                               kwargs=kwargs)
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        return Response(retn)


class DownloadJob(CreateRetrieveAPIView):
    queryset = None
    serializer_class = None
    permission_classes = None
    task_name = None

    def __init__(self, *args, **kwargs):
        self.queryset = JobFilePath.objects.all()
        self.serializer_class = JobFilePathSerializer
        self.permission_classes = (permissions.AllowAny,)
        self.task_name = 'ythub_workers.tasks.get_job_filepath_docker'

    def build_kwargs(self, obj):
        return {'cont_id': [obj.job.container_id],
                'path': [obj.path]}

    def list(self, request, job_pk=None):
        objects = self.queryset.filter(job=job_pk)
        ser = self.serializer_class(objects, many=True)
        return Response(ser.data)

    def send_job(self, job_pk, path):
        data = {'job': job_pk, 'path': path}

        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            serializer.save()
            self.post_save(serializer.object, True)
            return {'valid': True, 'serializer': serializer}

        return {'valid': False, 'serializer': serializer}

    def create(self, request):
        if 'job_id' in request.QUERY_PARAMS:
            job_pk = request.QUERY_PARAMS['job_id']
        else:
            return Response("No Job ID Specified",
                            status=status.HTTP_400_BAD_REQUEST)

        if 'path' in request.QUERY_PARAMS:
            path = request.QUERY_PARAMS['path']
        else:
            return Response("No path Specified",
                            status=status.HTTP_400_BAD_REQUEST)

        ret = self.send_job(job_pk, path)
        if ret['valid']:
            return Response(ret['serializer'].data,
                            status=status.HTTP_201_CREATED)
        return Response(ret['serializer'].errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request):
        if 'job_id' in request.QUERY_PARAMS:
            job_pk = request.QUERY_PARAMS['job_id']
        else:
            return Response("No Job ID Specified",
                            status=status.HTTP_400_BAD_REQUEST)

        if 'path' in request.QUERY_PARAMS:
            path = request.QUERY_PARAMS['path']
        else:
            return Response("No path Specified",
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            object = self.queryset.get(job_pk=job_pk)
            ser = self.serializer_class(object)
            return Response(ser.data)
        except:
            ret = self.send_job(job_pk, path)
            if ret['valid']:
                return Response(ret['serializer'].data,
                                status=status.HTTP_200_OK)
            return Response(ret['serializer'].errors,
                            status=status.HTTP_400_BAD_REQUEST)

    def post_save(self, obj, created):
        kwargs = self.build_kwargs(obj)
        kwargs['worker'] = obj.job.worker

        ret = celery.send_task(self.task_name, kwargs=kwargs)
        obj.task_id = ret.id
        try:
            retn = simplejson.loads(ret.get())
        except Exception as e:
            retn = e.message
        obj.data = retn
        obj.save()
