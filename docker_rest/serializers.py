__author__ = 'keyz'
from rest_framework import serializers
from django.contrib.auth.models import User

from docker_rest.models import \
    Job, JobFilePath


class UserSerializer(serializers.ModelSerializer):
    jobs = serializers.PrimaryKeyRelatedField(many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'jobs')


class JobSerializer(serializers.ModelSerializer):
    owner = serializers.Field(source='owner.username')

    class Meta:
        model = Job
        fields = ('id', 'scriptname', 'script', 'image_name', 'image_tag',
                  'worker', 'create_task_id', 'create_task_state',
                  'container_id', 'owner')


class JobFilePathSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobFilePath
        fields = ('id', 'created', 'data', 'job', 'path')
