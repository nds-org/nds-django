from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers

from docker_rest import views as docker_rest_views

# Create a router and register our viewsets with it.
# apirouter = DefaultRouter()
# apirouter.register(r'jobs', docker_rest_views.JobViewSet)
# apirouter.register(r'users', docker_rest_views.UserViewSet)
# apirouter.register(r'irods', docker_rest_views.IrodsViewSet)
#
# metrics_router = routers.NestedSimpleRouter(apirouter, r'jobs', lookup='job')
# metrics_router.register(r'metrics', docker_rest_views.JobMetricsViewSet, base_name='metrics')
#
# state_router = routers.NestedSimpleRouter(apirouter, r'jobs', lookup='job')
# state_router.register(r'state', docker_rest_views.JobStateViewSet, base_name='state')
#
# stdout_router = routers.NestedSimpleRouter(apirouter, r'jobs', lookup='job')
# stdout_router.register(r'stdout', docker_rest_views.JobStdOutViewSet, base_name='stdout')
#
# change_router = routers.NestedSimpleRouter(apirouter, r'jobs', lookup='job')
# change_router.register(r'change', docker_rest_views.JobChangeViewSet, base_name='change')
#
# filepath_router = routers.NestedSimpleRouter(apirouter, r'jobs', lookup='job')
# filepath_router.register(r'filepath', docker_rest_views.JobFilePathViewSet, base_name='filepath')


urlpatterns = patterns('',
    # Examples:
    url(r'^$', include('home.urls', namespace='home')),
    url(r'^v1/', include('docker_rest.urls')),
    # url(r'^api/', include(apirouter.urls)),
    # url(r'^api/', include(metrics_router.urls)),
    # url(r'^api/', include(state_router.urls)),
    # url(r'^api/', include(stdout_router.urls)),
    # url(r'^api/', include(change_router.urls)),
    # url(r'^api/', include(filepath_router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
)
